math = require('mathjs');
fs = require('fs');
consola = require('consola');
const iterations = 100;
const stepSize = 0.01;
//let max = 0;

function test(cmpx){
	if ((cmpx.im > 2) || (cmpx.im < -2))
		return false;
	if ((cmpx.re > 2) || (cmpx.re < -2))
		return false;
	return true;
}

function foo (cmpx) {
	let cur = math.complex(0, 0);
	for(let i = 0; i < iterations; i++){
		cur = math.add(math.square(cur) , cmpx);
	//	cur = math.subtract(math.square(cur) , cmpx);	// flip the set along its asymetric axis
		if (!test(cur))
			return iterations - i;
	}
	return 0;
}

let map = [];

function main () {
	consola.info(`START ${new Date}`);
	for(let i = -2.0; i < 2.0; i+=stepSize){
		let row = [];
		for (let k = -2.0; k < 2.0; k+=stepSize){
			let cmpx = math.complex(i, k);
			let tmp = foo(cmpx);
		//	if (tmp > max)
		//		max = tmp;
			row.push(tmp);
		}
		map.push(row); 
	}

	consola.info(`END ${new Date}`);
	consola.info(`Creating string`);
	let ppmstr = `P3\n#William Doyle | iterations ${iterations}\n${4*(1/stepSize)}\n${4*(1/stepSize)}\n${iterations}\n`;
	//let ppmstr = `P5\n${4*(1/stepSize)}\t${4*(1/stepSize)}\n${max}\n`;
	//let ppmstr = `P1\n${4*(1/stepSize)}\t${4*(1/stepSize)}\n`;
	
	map.map(row => {
		row.map(val => {
			ppmstr += (val === -1)? `100\n0\n0\n`:`${val}\n${val}\n${val}\n`;
		})
	});

//	consola.info(`max was ${max}`);
	let fname = `${new Date()}.ppm`;
	consola.info(`writing to \"${fname}\"`);
	fs.writeFile(fname, ppmstr, err => consola.error(err));
}

main();
