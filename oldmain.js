math = require('mathjs');
fs = require('fs');
const iterations = 100;
const stepSize = 0.01;

function test(cmpx){
	if ((cmpx.im > 2) || (cmpx.im < -2))
		return false;
	if ((cmpx.re > 2) || (cmpx.re < -2))
		return false;
	return true;
}

function foo (cmpx) {
	let cur = math.complex(0, 0);
	for(let i = 0; i < iterations; i++){
		cur = math.add(math.square(cur) , cmpx);
	//	cur = math.subtract(math.square(cur) , cmpx);	// flip the set along its asymetric axis
		if (!test(cur))
			return false;
	}
	return test(cur);
}

let map = [];

function main () {
	for(let i = -2.0; i < 2.0; i+=stepSize){
		let row = [];
		for (let k = -2.0; k < 2.0; k+=stepSize){
			let cmpx = math.complex(i, k);
			row.push(foo(cmpx));
		}
		map.push(row); 
	}

	let ppmstr = `P1\n#William Doyle | iterations ${iterations} \n${4*(1/stepSize)}\t${4*(1/stepSize)}\n`;
	//let ppmstr = `P1\n#William Doyle | iterations ${iterations} \n${4001}\t${4001}\n`;
//	let ppmstr = `P1\n#William Doyle | iterations ${iterations} \n${400}\t${400}\n`;
	map.map(row => {
		row.map(val => {
			ppmstr += `\n${val? '1':'0'}`;
		})
	});
	
	fs.writeFile(`${new Date()}.pbm`, ppmstr, err => console.log(err));
}

main();
