math = require('mathjs');
fs = require('fs');
const iterations = 100;
const stepSize = 0.01;

function test(cmpx){
	if ((cmpx.im > 2) || (cmpx.im < -2))
		return false;
	if ((cmpx.re > 2) || (cmpx.re < -2))
		return false;
	return true;
}

function foo (cmpx) {
	let cur = math.complex(0, 0);
	for(let i = 0; i < iterations; i++){
		cur = math.add(math.square(cur) , cmpx);
	//	cur = math.subtract(math.square(cur) , cmpx);	// flip the set along its asymetric axis
		if (!test(cur))
			return false;
	}
	return test(cur);
}

let map = [];

async function mandelRange(starti, startk, endi, endk, arr){
	for(let i = starti; i < endi; i+=stepSize){
		let row = [];
		for (let k = startk; k < endk; k+=stepSize){
			let cmpx = math.complex(i, k);
			row.push(foo(cmpx));
		}
		arr.push(row); 
	}
}

// Remove this comment when you understand that is not multithreaded and have made it actually multithreaded
async function main () {

	let starti = -2.0;
	let endi = 2.0;
	let startk = -2.0;
	let endk = 2.0;

	let parta = [];
	let partb = [];
	mandelRange(-2.0, startk, 0, endk, parta);
	mandelRange(0, startk, 2.0, endk, partb);
	map = parta.concat(partb);

	let ppmstr = `P1\n#William Doyle | iterations ${iterations} \n${4*(1/stepSize)}\t${4*(1/stepSize)}\n`;
	
	map.map(row => {
		row.map(val => {
			ppmstr += `\n${val? '1':'0'}`;
		})
	});
	


	fs.writeFile(`${new Date()}.pbm`, ppmstr, err => console.log(err));
}

main();
